#!/bin/bash

# generate a 2048-bit RSA private key
openssl genrsa -out private_key.pem 2048
# convert private Key to PKCS#8 format (so Java can read it)
openssl pkcs8 -topk8 -inform PEM -outform DER -in private_key.pem -out private_key.der -nocrypt
# Clé publique
# output public key portion in DER format (so Java can read it)
openssl rsa -in private_key.pem -pubout -outform DER -out public_key.der
# Certificat pour ASP.Net Core (entrez les informations nécessaires)
openssl req -new -key private_key.pem -out public_cert.csr
openssl x509 -req -days 365 -in public_cert.csr -signkey private_key.pem -out public_cert.crt


# Vérifier un certificat
# openssl x509 -in public_cert.crt -text -noout
