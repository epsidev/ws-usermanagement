package fr.epsi.montpellier.wsusermanagement.security;

public class AuthenticationRequest {
    public String username;
    public String password;
    public String role;	// Rôle demandé pour l'authentification (paramètre optionnel)

}
