package fr.epsi.montpellier.wsusermanagement.security;

public class AuthenticationResponse {
    public boolean status;
    public String token;
    public String message;
}
