package fr.epsi.montpellier.wsusermanagement.services;

import fr.epsi.montpellier.Ldap.UserLdap;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class JwtTokenService {

    @Value("${jwt.expire.hours}")
    private int expireHours;

    @Value("${jwt.token.private_key}")
    private String privateKeyRessource;
    @Value("${jwt.token.public_key}")
    private String publicKeyRessource;

    public String createToken(UserLdap user) {
        // Génération de la date d'expiration
        LocalDateTime expireIn = LocalDateTime.now().plusHours(expireHours);

        // Construction des attributs (claims) pour le token
        Map<String, Object> claims = populateClaims(user);

        // Construction du token avec la clé privée
        return Jwts.builder()
                .signWith(getPrivateKey()) //, SignatureAlgorithm.RS512)
                .claims(claims)
                .issuedAt(new Date())
                .expiration(Date.from(expireIn.atZone(ZoneId.systemDefault()).toInstant()))
                .compact();
    }

    public Jws<Claims> validateToken(String authToken) {
        return Jwts.parser()
                .verifyWith(getPublicKey())
                .build()
                .parseSignedClaims(authToken);
    }

    private Map<String, Object> populateClaims(UserLdap user) {
        Map<String, Object> map = new HashMap<>();
        map.put(Claims.SUBJECT, user.getLogin());
        map.put("nom", user.getNom());
        map.put("prenom", user.getPrenom());
        map.put("mail", user.getMail());

        map.put("classe", user.getClasse());
        map.put("bts", user.isBts());
        map.put("btsparcours", user.getBtsParcours());
        map.put("btsnumero", user.getBtsNumero());

        String roles = user.getRole();
        if (! roles.isEmpty()) {
            map.put("roles", Arrays.asList(roles.split(",")));
        }
        return map;
    }

    private Key getPrivateKey() {
        // Création de la clé
        byte[] privateKey = readPrivateKey();
        if (privateKey != null) {
            try {
                PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(privateKey);

                KeyFactory kf = KeyFactory.getInstance("RSA");
                return kf.generatePrivate(spec);
            } catch (Exception ex) {
                System.err.println("d.k.w.s.JwtTokenService.getPrivateKey, " + ex.getMessage());
            }
        }
        return null;
    }

    private byte[] readPrivateKey() {
        return readKey(privateKeyRessource);
    }

    private PublicKey getPublicKey() {
        // Création de la clé
        byte[] publicKey = readPublicKey();
        if (publicKey != null) {
            try {
                X509EncodedKeySpec spec = new X509EncodedKeySpec(publicKey);

                KeyFactory kf = KeyFactory.getInstance("RSA");
                return kf.generatePublic(spec);
            } catch (Exception ex) {
                System.err.println("d.k.w.s.JwtTokenService.getPublicKey, " + ex.getMessage());
            }
        }
        return null;
    }
    private byte[] readPublicKey() {
        return readKey(publicKeyRessource);
    }

    private static byte[] readKey(String ressourceName) {
        try {
            // Le fichier se trouve à l'extérieur du JAR
            return Files.readAllBytes(Path.of(ressourceName));
        } catch (Exception ex) {
            System.err.println("d.k.w.s.JwtTokenService.readKey, " + ex.getMessage());
        }

        return null;
    }
}
