package fr.epsi.montpellier.wsusermanagement.api.controller;


import fr.epsi.montpellier.wsusermanagement.security.AuthenticationRequest;
import fr.epsi.montpellier.wsusermanagement.security.AuthenticationResponse;
import fr.epsi.montpellier.wsusermanagement.services.JwtTokenService;
import fr.epsi.montpellier.wsusermanagement.services.LdapManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

import fr.epsi.montpellier.Ldap.UserLdap;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController
{
    // Les classes de service
    private final JwtTokenService jwtTokenService;
    private LdapManagerService service;
    private final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    //@Autowired est fait automatiquement
    public AuthenticationController(LdapManagerService service, JwtTokenService tokenService) {
        this.service = service;
        jwtTokenService = tokenService;
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse>  authenticate(@Valid @RequestBody AuthenticationRequest authenticationRequest) {
        // La réponse pour l'authentification
        AuthenticationResponse response = new AuthenticationResponse();
        // par défaut, cela se passera mal !
        response.status = false;

        // Juste pour le debug
        logger.info(String.format("AuthenticationController.authenticate Login=%s, Role=%s", authenticationRequest.username, authenticationRequest.role));

        // Authentification openLDAP
        try {
            UserLdap user = service.getManager().authenticateUser(authenticationRequest.username, authenticationRequest.password);
            if (user != null) {
                // Si un rôle est demandé pour l'authentification ...
                if (StringUtils.hasLength(authenticationRequest.role)) {
                    // ... alors si le role fourni est présent dans la liste des roles, on accepte l'authentification
                    if (user.getRole().contains(authenticationRequest.role.toUpperCase()))
                        response.status = true;
                    // sinon, elle est implicitement refusée
                } else {
                    // Si aucun rôle n'est demandé alors on accepte l'authentification
                    response.status = true;
                }
            }

            if (response.status) {
                // Création du token d'après l'utilisateur
                response.token = jwtTokenService.createToken(user);
                // Envoi de la réponse
                return ResponseEntity.ok().body(response);
            }
        } catch (Exception ex) {
            logger.error("AuthenticationController.authenticate: {}", ex.getMessage());
        }
        logger.info("L'authentification a échoué");

        // L'authentification a échoué
        throw new ResponseStatusException(
                HttpStatus.UNAUTHORIZED,
                HttpStatus.UNAUTHORIZED.getReasonPhrase());
    }

}
